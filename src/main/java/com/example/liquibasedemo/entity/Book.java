package com.example.liquibasedemo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Book {

	@Id
	private Long bookId;

	@Column(nullable = false)
	private String title;
}
